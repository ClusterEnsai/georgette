# -*- coding: utf-8 -*-
""" *************************************************************************** 
         ________    __  _______________________
        / ____/ /   / / / / ___/_  __/ ____/ __ \
       / /   / /   / / / /\__ \ / / / __/ / /_/ /
      / /___/ /___/ /_/ /___/ // / / /___/ _, _/
      \____/_____/\____//____//_/ /_____/_/ |_|

 Created on Fri Jul 24 09:13:44 2020
 
 @author: giver
 
*************************************************************************** """

from kheylogz import cereales

print("Type :")
print(type(cereales))

print("\nType contenu :")
print(cereales.dtype)

print("\nDimensions :")
print(cereales.shape)