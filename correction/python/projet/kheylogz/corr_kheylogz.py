# -*- coding: utf-8 -*-
""" *************************************************************************** 
         ________    __  _______________________
        / ____/ /   / / / / ___/_  __/ ____/ __ \
       / /   / /   / / / /\__ \ / / / __/ / /_/ /
      / /___/ /___/ /_/ /___/ // / / /___/ _, _/
      \____/_____/\____//____//_/ /_____/_/ |_|

 Created on Tue Dec  3 14:29:51 2019
 
 @author: giver
 
*************************************************************************** """

import numpy as np

""" Import des données """
c1 = np.loadtxt("cereales1.csv", delimiter=",", dtype = str)
c2 = np.loadtxt("cereales2.csv", delimiter=",", skiprows = 1)
c3 = np.loadtxt("cereales3.csv", delimiter=",", skiprows = 1)
c4 = np.loadtxt("cereales4.csv", delimiter=",", skiprows = 1)

c1 = c1.T[1:,:].astype(float)

""" reverse c2 """

c2 = c2[np.arange(0, 63)[::-1],:]

""" merge c2 & c3 """

c2c3 = np.array([c3[:, 0],
                 c2[:, 1],
                 c3[:,2],
                 c2[:,2],
                 c3[:,4],
                 c2[:,3],
                 c2[:,4],
                 c3[:, 1],
                 c3[:, -1],
                 c3[:, 3]]).T

""" stack c1 & c2c3 """

cereales = np.vstack((c1, c2c3))

cereales = np.vstack((cereales[:32,:],
                      c4[0,:].reshape((1, 10)),
                      cereales[32:63,:],
                      c4[1,:].reshape((1, 10)),
                      cereales[63:,:]))

""" Calcul des moyennes pour l'imputation et les rangs """
#Moyennes en enlevant les valeurs manquantes
meanCalories = round(np.mean(cereales[cereales[:,1] != -1,1]))
meanGlucides = round(np.mean(cereales[cereales[:,6] != -1,6]))
meanSucres = round(np.mean(cereales[cereales[:,7] != -1,7]))
meanPotassium = round(np.mean(cereales[cereales[:,8] != -1,8]))
#Pas de valeurs manquantes
meanFibres = round(np.mean(cereales[:,5]))
meanVitamines = round(np.mean(cereales[:,9]))


""" Imputation """
cereales[cereales[:,1] == -1,1] = meanCalories
cereales[cereales[:,6] == -1,6] = meanGlucides
cereales[cereales[:,7] == -1,7] = meanSucres
cereales[cereales[:,8] == -1,8] = meanPotassium

""" Ajout colonne à remplir """

cereales = np.hstack((cereales, np.zeros(77).reshape((77, 1))))

""" Remplissage des rangs """
cereales[cereales[:,1] > 110,-1] = 5
cereales[(cereales[:,1] <= 110) & (cereales[:,1] > 100) & (cereales[:,7] > meanSucres),-1] = 4
cereales[(cereales[:,1] <= 110) & (cereales[:,1] > 100) & (cereales[:,7] <= meanSucres),-1] = 3
cereales[((cereales[:,5] <= meanFibres) | (cereales[:,8] <= meanPotassium) | (cereales[:,9] <= meanVitamines)) & (cereales[:,1] <= 100),-1] = 2
cereales[(cereales[:,5] > meanFibres) & (cereales[:,8] > meanPotassium) & (cereales[:,9] > meanVitamines) & (cereales[:,1] <= 100),-1] = 1

""" Drop de l'identifiant """
cereales = cereales[:,1:]





